关于PSI
-------------
>PSI是开源进销存系统。PSI是Purchasing Sale Invertory这三个单词的首字母。

PSI演示
-------------
>PSI的演示见：http://psi.oschina.mopaas.com

PSI 2015 beta1下载地址
-------------
>PSI 2015 beta1下载地址见：http://pan.baidu.com/s/18krym

PSI的开源协议
-------------
>PSI的开源协议为GPL v3

>如果您有Sencha Ext JS的商业许可（参见： http://www.sencha.com/products/extjs/licensing/ ），那么PSI的开源协议为Apache License v2。

>GPL v3协议下，您必须保持PSI代码的开源。 
>Apache License协议下，您可以闭源并私有化PSI的代码，作为您自己的商业产品来销售。

如需要技术支持，请联系
-------------
>QQ：1569352868
>
>Email：1569352868@qq.com
>
>QQ群：414474186

致谢
-------------
>PSI使用了如下开源软件，没有你们，就没有PSI
> 
>1、PHP (http://php.net/)
>
>2、MySQL (http://www.mysql.com/)
>
>3、ExtJS 4.2 (http://www.sencha.com/)
>
>4、ThinkPHP 3.2.3 (http://www.thinkphp.cn/)
>
>5、Overtrue\Pinyin (http://overtrue.me/pinyin/)
>
>6、PHPExcel (http://phpexcel.codeplex.com/)
