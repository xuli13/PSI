<?php

namespace Home\Common;

/**
 * FId常数值
 *
 * @author Administrator
 */
class FIdConst {
	
	/**
	 * 首页
	 */
	const HOME = "-9997";
	
	/**
	 * 重新登录
	 */
	const RELOGIN = "-9999";
	
	/**
	 * 修改我的密码
	 */
	const CHANGE_MY_PASSWORD = "-9996";
	
	/**
	 * 用户管理
	 */
	const USR_MANAGEMENT = "-8999";
	
	/**
	 * 权限管理
	 */
	const PERMISSION_MANAGEMENT = "-8996";
	
	/**
	 * 业务日志
	 */
	const BIZ_LOG = "-8997";
	
	/**
	 * 基础数据-仓库
	 */
	const WAREHOUSE = "1003";
	
	/**
	 * 基础数据-供应商档案
	 */
	const SUPPLIER = "1004";
	
	/**
	 * 基础数据-商品
	 */
	const GOODS = "1001";
	
	/**
	 * 基础数据-商品计量单位
	 */
	const GOODS_UNIT = "1002";
	
	/**
	 * 客户资料
	 */
	const CUSTOMER = "1007";
	
	/**
	 * 库存建账
	 */
	const INVERTORY_INIT = "2000";
	
	/**
	 * 采购入库
	 */
	const PURCHASE_WAREHOUSE = "2001";
	
	/**
	 * 库存账查询
	 */
	const INVERTORY_QUERY = "2003";
	
	/**
	 * 应付账款管理
	 */
	const PAYABLES = "2005";
	
	/**
	 * 应收账款管理
	 */
	const RECEIVING = "2004";
	
	/**
	 * 销售出库
	 */
	const WAREHOUSING_SALE = "2002";
	
	/**
	 * 销售退货入库
	 */
	const SALE_REJECTION = "2006";
	
	/**
	 * 业务设置
	 */
	const BIZ_CONFIG = "2008";
}
